<?php

class migration_00002_movies
{

    public function up()
    {
        $db = \app\core\Application::$app->database;
        $SQL = "CREATE TABLE movies(
                id INT AUTO_INCREMENT PRIMARY KEY,
                title VARCHAR(100) NOT NULL ,
                release_year int(11) NOT NULL ,
                format VARCHAR(10) NOT NULL,   
                created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP
                ) ENGINE=INNODB";
        $db->PDO->exec($SQL);
    }

    public function down()
    {
        $db = \app\core\Application::$app->database;
        $SQL = "DROP TABLE IF EXISTS movies;";
        $db->PDO->exec($SQL);
    }
}