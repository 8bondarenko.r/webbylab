<?php

class migration_00004_movies_has_stars
{

    public function up()
    {
        $db = \app\core\Application::$app->database;
        $SQL = "CREATE TABLE movies_has_stars(
                id INT AUTO_INCREMENT PRIMARY KEY,
                id_movie int(11) NOT NULL ,
                id_star int(11) NOT NULL
                ) ENGINE=INNODB";
        $db->PDO->exec($SQL);
    }

    public function down()
    {
        $db = \app\core\Application::$app->database;
        $SQL = "DROP TABLE IF EXISTS movies_has_stars;";
        $db->PDO->exec($SQL);
    }
}