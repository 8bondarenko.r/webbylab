<?php

class migration_00003_stars
{

    public function up()
    {
        $db = \app\core\Application::$app->database;
        $SQL = "CREATE TABLE stars(
                id INT AUTO_INCREMENT PRIMARY KEY,
                name VARCHAR(50) NOT NULL,   
                created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP
                ) ENGINE=INNODB";
        $db->PDO->exec($SQL);
    }

    public function down()
    {
        $db = \app\core\Application::$app->database;
        $SQL = "DROP TABLE IF EXISTS stars;";
        $db->PDO->exec($SQL);
    }
}