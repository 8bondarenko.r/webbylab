<?php

class migration_00001_init
{

    public function up()
    {
        $db = \app\core\Application::$app->database;
        $SQL = "CREATE TABLE users(
                id INT AUTO_INCREMENT PRIMARY KEY,
                email VARCHAR(100) NOT NULL ,
                name VARCHAR(50) NOT NULL ,
                password VARCHAR(255) NOT NULL,   
                created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP
                ) ENGINE=INNODB";
        $db->PDO->exec($SQL);
    }

    public function down()
    {
        $db = \app\core\Application::$app->database;
        $SQL = "DROP TABLE IF EXISTS users;";
        $db->PDO->exec($SQL);
    }
}