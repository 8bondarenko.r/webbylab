<?php


namespace app\src\models;


use app\core\DbModel;

class Stars extends DbModel
{
    public string $name = '';

    public function tableName(): string
    {
        return 'stars';
    }

    public function attributes(): array
    {
        return ['name'];
    }

    public function primaryKey(): string
    {
        return 'id';
    }

    public function labels(): array
    {
        return [
            'name' => 'Name'
        ];
    }

    protected function rules(): array
    {
        return [
            'name' => [self::RULE_REQUIRE],
        ];
    }

    /**
     * @param $stars
     * @return array
     */
    public function insertStar($stars): array
    {
        if(is_string($stars)){
            $stars = explode(',', $stars);
        }
        $lastIds = [];

        foreach ($stars as $star) {
            $star = trim($star);
            if($star === '') continue;
            $checkStar = $this->findOne(['name' => $star]);
            if (!$checkStar) {
                $this->loadData(['name' => $star]);
                $this->save();
                $lastIds[] = $this->getLastId();
            } else {
                $lastIds[] = $checkStar->id;
            }
        }

        return $lastIds;
    }
}