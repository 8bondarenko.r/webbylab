<?php


namespace app\src\models;


use app\core\Application;
use app\core\DbModel;

class MoviesHasStars
{
    public function tableName(): string
    {
        return 'movies_has_stars';
    }

    public function attributes(): array
    {
        return ['id_movie', 'id_star'];
    }

    public function createRelation($id_movie, $ids_stars)
    {
        foreach ($ids_stars as $id_star) {
            $tableName = $this->tableName();
            $attributes = $this->attributes();
            $statement = $this->prepare("INSERT INTO $tableName (" . implode(",", $attributes) . ") 
                VALUES ({$id_movie}, {$id_star})");
            $statement->execute();
        }
    }


    /**
     * @param $sql
     * @return false|\PDOStatement
     */
    public function prepare($sql)
    {
        return Application::$app->database->PDO->prepare($sql);
    }
}