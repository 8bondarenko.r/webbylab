<?php


namespace app\src\models;


use app\core\Application;
use app\core\DbModel;

class Movies extends DbModel
{
    public string $title = '';
    public string $release_year = '';
    public string $format = '';
    public string $stars = '';

    public function attributes(): array
    {
        return ['title', 'release_year', 'format'];
    }

    public function tableName(): string
    {
        return 'movies';
    }
    public function labels(): array
    {
        return [
            'title'=> 'Title',
            'release_year'=> 'Release year',
            'format'=> 'Format',
        ];
    }

    /**
     * @param $data
     * @return string
     */
    public function insertMovie($data): string
    {
        $data['title'] = trim($data['title']);
        $checkMovie = $this->findOne(['title' => $data['title']]);
        if(!$checkMovie) {
            $this->loadData($data);
            $this->save();
            return $this->getLastId();
        }

        return $checkMovie->id;
    }

    /**
     * @return array
     */
    public function getMoviesWithStarts(): array
    {
        $statement = $this->prepare("SELECT id, title, release_year, format FROM movies");
        $statement->execute();
        $result = $statement->fetchAll(Application::$app->database->PDO::FETCH_ASSOC);
        foreach ($result as &$item) {
            $relation = $this->prepare("SELECT id_star FROM movies_has_stars WHERE id_movie = {$item['id']}");
            $relation->execute();
            $stars_ids = $relation->fetchAll(Application::$app->database->PDO::FETCH_COLUMN);
            $implode_ids = implode(',',$stars_ids);
            if($implode_ids === '') {
                continue;
            }
            $get_stars = $this->prepare("SELECT GROUP_CONCAT(name SEPARATOR ', ') as name FROM stars WHERE id IN ($implode_ids)");
            $get_stars->execute();
            $item['stars'] = $get_stars->fetchAll(Application::$app->database->PDO::FETCH_COLUMN);
        }

        return $result;
    }

    public function removeMovie(array $getBody)
    {
        $deleteMovie = $this->prepare("DELETE FROM movies where id={$getBody['delete_movie']}");
        $deleteMovie->execute();
        $deleteRelation = $this->prepare("DELETE FROM movies_has_stars where id_movie={$getBody['delete_movie']}");
        $deleteRelation->execute();
    }

    protected function rules(): array
    {
        return [
            'title' => [self::RULE_REQUIRE,[
                self::RULE_UNIQUE, 'class' => self::class,
            ]],
            'release_year' => [self::RULE_REQUIRE,
                [self::RULE_MIN_VALUE, 'min_value' => 1850],
                [self::RULE_MAX_VALUE, 'max_value' => 2022],
            ],
            'format' => [self::RULE_REQUIRE],
        ];
    }


    public function primaryKey(): string
    {
        return 'id';
    }
}