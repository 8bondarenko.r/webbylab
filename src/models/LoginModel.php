<?php


namespace app\src\models;


use app\core\Application;
use app\core\Model;

class LoginModel extends Model
{
    public string $email = '';
    public string $password = '';

    /**
     * @return false
     */
    public function login(): bool
    {
        $user = (new User())->findOne(['email' => $this->email]);

        if(!$user) {
            $this->addError('email', 'User does not exist with this email');
            return false;
        }

        if(!password_verify($this->password, $user->password )) {
            $this->addError('password', 'Invalid password');
            return false;
        }

        return Application::$app->login($user);
    }

    public function labels()
    {
        return [
            'email'=> 'Email',
            'password'=> 'Password',
        ];
    }

    protected function rules(): array
    {
        return [
            'email' => [self::RULE_REQUIRE, self::RULE_EMAIL],
            'password' => [self::RULE_REQUIRE],
        ];
    }
}