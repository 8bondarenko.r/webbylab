<?php

namespace app\src\models;



use app\core\UserModel;

class User extends UserModel
{
    public string $name = '';
    public string $email = '';
    public string $password = '';
    public string $confirm_password = '';

    public function attributes(): array
    {
        return ['name', 'email', 'password'];
    }

    public function tableName(): string
    {
        return 'users';
    }

    public function primaryKey(): string
    {
       return 'id';
    }

    public function save()
    {
        $this->password = password_hash($this->password, PASSWORD_DEFAULT);
        return parent::save();
    }

    public function rules(): array
    {
        return [
            'name' => [self::RULE_REQUIRE],
            'email' => [self::RULE_REQUIRE, self::RULE_EMAIL, [
                self::RULE_UNIQUE, 'class' => self::class,
            ]],
            'password' => [self::RULE_REQUIRE,
                [self::RULE_MIN, 'min' => 4],
                [self::RULE_MAX, 'max' => 8],
            ],
            'confirm_password' => [self::RULE_REQUIRE,
                [self::RULE_MATCH, 'match' => 'password'],
            ],

        ];
    }

    public function labels(): array
    {
        return [
            'name'=> 'Name',
            'email'=> 'Email',
            'password'=> 'Password',
            'confirm_password'=> 'Repeat password',
        ];
    }

    public function name(): string
    {
        return $this->name;
    }
}