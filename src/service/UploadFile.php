<?php


namespace app\src\service;


use app\core\Application;

class UploadFile
{
    /**
     * @param $file
     * @return array
     */
    public function refactorFile($file): array
    {
        if((int)$file['size'] === 0) {
            Application::$app->session->setFlash('error', "Empty {$file['name']}");
            Application::$app->response->redirect('/');
            exit();

        }
        $movies = explode("\n\n",trim(file_get_contents($file['tmp_name'], true)));
        $movie_filtered_data = [];
        $all_movies = [];
        foreach ($movies as $movie) {
            $movie_fields = explode("\n", $movie);
            foreach ($movie_fields as $field) {
                list($key, $value) = explode(": ", $field);
                $key = strtolower(str_replace(' ', '_', $key));
                if($key === 'stars') {
                    $value = explode(", ", $value);
                }
                $movie_filtered_data[$key] = $value;
            }
            $all_movies[] = $movie_filtered_data;
        }
        return $all_movies;
    }


}