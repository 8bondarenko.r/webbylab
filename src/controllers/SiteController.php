<?php

namespace app\src\controllers;

use app\core\Application;
use app\core\BaseController;
use app\core\Request;
use app\core\Response;
use app\src\models\Movies;
use app\src\models\MoviesHasStars;
use app\src\models\Stars;
use app\src\service\UploadFile;

/**
 * Class SiteController
 * @package app\controllers
 */
class SiteController extends BaseController
{
    private Movies $movies;
    private Stars $stars;
    private MoviesHasStars $moviesHasStars;

    public function __construct()
    {
        $this->movies = new Movies();
        $this->stars = new Stars();
        $this->moviesHasStars = new MoviesHasStars();
    }

    public function home()
    {
        return $this->render('home', [
            'name' => Application::$app->isGuest() ? '' : Application::$app->user->name(),
            'moviesWithStarts' => $this->movies->getMoviesWithStarts()
        ]);
    }

    public function upload(Request $request, Response $response)
    {
        $service = new UploadFile();
        if ($request->isPost() && $request->isUploadedFile()) {
            try {
                $refactorFile = $service->refactorFile($request->getFile());
                foreach ($refactorFile as $item) {
                    $insertMovie = $this->movies->insertMovie($item);
                    $insertStar = $this->stars->insertStar($item['stars']);
                    $this->moviesHasStars->createRelation($insertMovie, $insertStar);
                    $response->redirect('/');
                }
            } catch (\Exception $e) {
                Application::$app->session->setFlash('error', "{$e->getCode()}, {$e->getMessage()}");
                return $this->render('upload', [
                    'model' => $this->movies
                ]);
            }
        }

        return $this->render('upload', [
            'model' => $this->movies
        ]);
    }

    public function addMovie(Request $request, Response $response)
    {
        if ($request->isPost()){
            $this->movies->loadData($request->getBody());
            if ($this->movies->validate()) {
                $data = $request->getBody();
                $insertMovie = $this->movies->insertMovie($data);
                if($data['stars'] !== ''){
                    $insertStar = $this->stars->insertStar($data['stars']);
                    $this->moviesHasStars->createRelation($insertMovie, $insertStar);
                }
                Application::$app->session->setFlash('success', "Movie {$data['title']} was added");
                $response->redirect('/');
            }

        }
        return $this->render('add_movie', [
            'model' => $this->movies
        ]);
    }

    public function removeMovie(Request $request, Response $response)
    {
        if ($request->isPost()){
            $this->movies->removeMovie($request->getBody());
            $response->redirect('/');
        }
    }

}