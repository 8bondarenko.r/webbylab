<?php


namespace app\src\controllers;


use app\core\Application;
use app\core\BaseController;
use app\core\Request;
use app\core\Response;
use app\src\models\LoginModel;
use app\src\models\User;

class AuthController extends BaseController
{
    public function login(Request $request, Response $response)
    {
        $this->setLayout('auth');
        $loginModel = new LoginModel();
        if ($request->isPost()) {
            $loginModel->loadData($request->getBody());
            if ($loginModel->validate() && $loginModel->login()) {
                $response->redirect('/');
            }
        }
        $this->setLayout('auth');
        return $this->render('login', [
            'model' => $loginModel
        ]);
    }

    /**
     * @param Request $request
     * @param Response $response
     */
    public function logout(Request $request, Response $response)
    {
        Application::$app->logout();
        $response->redirect('/');
    }



    public function register(Request $request, Response $response)
    {
        $this->setLayout('auth');
        $userModel = new User();
        if ($request->isPost()) {
            $userModel->loadData($request->getBody());
            if ($userModel->validate() && $userModel->save()) {
                Application::$app->session->setFlash('success', 'Thanks for register');
                $response->redirect('/');
            }

            return $this->render('register', [
                'model' => $userModel
            ]);
        }

        return $this->render('register', [
            'model' => $userModel
        ]);
    }
}