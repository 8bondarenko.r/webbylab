<?php
/** @var $name */
/** @var $moviesWithStarts */
?>
<h1><?= $name?></h1>

<table id="table_id" class="display">
	<thead>
	<tr>
		<th>Id</th>
		<th>Title</th>
		<th>Release year</th>
		<th>Format</th>
		<th>Actors</th>
		<th>Delete movie</th>
		<th>Show info</th>
	</tr>
	</thead>
	<tbody>
	<?php foreach ($moviesWithStarts as $moviesWithStart):?>
	<tr>
		<td><?= $moviesWithStart['id']?></td>
		<td><?= $moviesWithStart['title']?></td>
		<td><?= $moviesWithStart['release_year']?></td>
		<td><?= $moviesWithStart['format']?></td>
		<td><?= $moviesWithStart['stars'][0] ?? ''?></td>
		<td>
			<button id="delete_movie" type="button" class="btn btn-danger" data-toggle="modal" data-target="#deleteMovie"
							data-film-id="<?= $moviesWithStart['id']?>">Remove
			</button>
		</td>
		<td>
			<button id="show-data" type="button" class="btn btn-info" data-toggle="modal" data-target="#exampleModal"
							data-title="<?= $moviesWithStart['title']?>"
							data-release_year="<?= $moviesWithStart['release_year']?>"
							data-format="<?= $moviesWithStart['format']?>"
							data-stars="<?= $moviesWithStart['stars'][0] ?? ''?>">
				Show info
			</button>
		</td>
	</tr>
	<?php endforeach;?>
	</tbody>
</table>

<!-- Modal Show info -->
<div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<div class="fetched-data"></div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>

<!-- Modal delete film-->
<div class="modal fade" id="deleteMovie" tabindex="-1" aria-labelledby="deleteMovieLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
					<p>Are you sure you want to delete the movie?</p>
          <?php $form = \app\core\form\Form::begin('/remove_movie', 'post')?>
				<input type="hidden" value="" name="delete_movie" id="film_id">
				<button type="submit" class="btn btn-danger">Yes</button>
				<button type="button" class="btn btn-success" data-dismiss="modal">No</button>
          <?php app\core\form\Form::end()?>
			</div>
		</div>
	</div>
</div>

<script>
    $(document).ready( function () {
        $('#table_id').DataTable();

        $("#table_id").on('click', '#show-data', function () {
            const title = $(this).data('title');
            const release_year = $(this).data('release_year');
            const format = $(this).data('format');
            const stars = $(this).data('stars');
            const str = "Title: " + title
                + "<br>Release year: " + release_year
                + "<br> Format: " + format
                + "<br> Lists of actors: " + stars;
            $(".fetched-data").html(str);
        })

        $("#table_id").on('click', '#delete_movie', function () {
            $("#film_id").val($(this).data('film-id'));
        })
    });
</script>