<?php /** @var $model \app\src\models\UploadFile*/?>

<?php $form = \app\core\form\Form::begin('/upload', 'post', 'enctype="multipart/form-data"')?>
<?= $form->field($model, 'file')->fieldTypeChange(\app\core\form\Field::TYPE_FILE)?>
<button type="submit" class="btn btn-primary">Submit</button>
<?php app\core\form\Form::end()?>
