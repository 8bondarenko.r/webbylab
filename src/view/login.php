<?php
/** @var $model \app\src\models\User */
///** @var $attribute */
?>
<h1>Login</h1>
<?php $form = \app\core\form\Form::begin('/login', 'post')?>
<?= $form->field($model, 'email')->fieldTypeChange(\app\core\form\Field::TYPE_EMAIL)?>
<?= $form->field($model, 'password')->fieldTypeChange(\app\core\form\Field::TYPE_PASSWORD)?>
<button type="submit" class="btn btn-primary">Submit</button>
<?php app\core\form\Form::end()?>