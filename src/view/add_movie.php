<h1>Add movie</h1>
<?php
/** @var $model \app\src\models\Movies */
?>
<?php $form = \app\core\form\Form::begin('/add_movie', 'post') ?>
<?= $form->field($model, 'title') ?>
<?= $form->field($model, 'release_year') ?>
<?= $form->field($model, 'format')->fieldTypeChange(\app\core\form\Field::TYPE_SELECT) ?>
<?= $form->field($model, 'stars')->fieldTypeChange(\app\core\form\Field::TYPE_TEXTAREA) ?>
<button type="submit" class="btn btn-primary">Submit</button>
<?php app\core\form\Form::end() ?>

<script>
    $(document).ready(function () {
        $('#title').on('input', function () {
            this.value = this.value.replace(/[^0-9a-zA-Zа-яёА-ЯЁ \s\\-]/g, '');
        })
        $('#release_year').on('input', function () {
            this.value = this.value.replace(/[^0-9\.]/g, '');
        })
        $('#stars').on('keyup', function () {
            this.value = this.value.replace(/[^a-zа-яё-ії\s\,\\-]/gi, '');
        })
    });
</script>
