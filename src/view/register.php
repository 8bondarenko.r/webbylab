<h1>Register</h1>
<?php
/** @var $model \app\src\models\User*/
///** @var $attribute */
?>
<?php $form = \app\core\form\Form::begin('/register', 'post')?>
	<?= $form->field($model, 'name')?>
	<?= $form->field($model, 'email')->fieldTypeChange(\app\core\form\Field::TYPE_EMAIL)?>
	<?= $form->field($model, 'password')->fieldTypeChange(\app\core\form\Field::TYPE_PASSWORD)?>
	<?= $form->field($model, 'confirm_password')->fieldTypeChange(\app\core\form\Field::TYPE_PASSWORD)?>
<button type="submit" class="btn btn-primary">Submit</button>
<?php app\core\form\Form::end()?>


