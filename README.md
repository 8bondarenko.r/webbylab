# WebbyLab



## Getting started

- git clone https://gitlab.com/8bondarenko.r/webbylab.git
- create file .env from .env.example
- create database
- enter to .env name   database, user, pass 
- composer install
- cd public
- php -S localhost:8080  
- php migrations.php
- registrate
- login 
- upload films

