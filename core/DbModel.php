<?php


namespace app\core;


abstract class DbModel extends Model
{
    /**
     * @return string
     */
    abstract public function tableName(): string;

    /**
     * @return array
     */
    abstract public function attributes(): array;

    /**
     * @return string
     */
    abstract public function primaryKey(): string;

    /**
     * @return array
     */
    abstract public function labels(): array;

    public function save()
    {
        $tableName = $this->tableName();
        $attributes = $this->attributes();
        $params = array_map(fn($attr) => ":$attr", $attributes);
        $statement = $this->prepare("INSERT INTO $tableName (" . implode(",", $attributes) . ") 
                VALUES (" . implode(",", $params) . ")");
        foreach ($attributes as $attribute) {
            $statement->bindValue(":$attribute", $this->{$attribute});
        }


        $statement->execute();
        return true;
    }

    /**
     * @param $where
     * @return mixed
     */
    public function findOne($where)
    {
        $tableName = static::tableName();
        $attributes = array_keys($where);
        $sql = implode(" AND ", array_map(fn($attr) => "$attr = :$attr", $attributes));
        $statement = $this->prepare("SELECT * FROM $tableName WHERE $sql");
        foreach ($where as $key => $item) {
            $statement->bindValue(":$key", $item);
        }
        $statement->execute();
        return $statement->fetchObject(static::class);
    }

    /**
     * @param $sql
     * @return false|\PDOStatement
     */
    public function prepare($sql)
    {
        return Application::$app->database->PDO->prepare($sql);
    }

    /**
     * @return string
     */
    public function getLastId(): string
    {
        return Application::$app->database->PDO->lastInsertId();
    }
}