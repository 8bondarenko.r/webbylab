<?php


namespace app\core\form;


use app\core\Model;

class Form
{
    /**
     * @param $action
     * @param $method
     * @param string $enctype
     * @return Form
     */
    public static function begin($action, $method, string $enctype =''): Form
    {
        echo sprintf("<form action='%s' method='%s' %s>", $action, $method, $enctype);
        return new Form();
    }


    public static function end()
    {
        echo "</form>";
    }

    /**
     * @param Model $model
     * @param $attribute
     * @return Field
     */
    public function field(Model $model, $attribute): Field
    {
        return new Field($model, $attribute);
    }


}