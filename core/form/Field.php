<?php


namespace app\core\form;

use app\core\Model;

/**
 * Class Field
 * @package app\core\form
 */
class Field
{
    public const TYPE_TEXT = 'text';
    public const TYPE_PASSWORD = 'password';
    public const TYPE_EMAIL = 'email';
    public const TYPE_FILE = 'file';
    public const TYPE_TEXTAREA = 'textarea';
    public const TYPE_SELECT = 'select';

    /**
     * @var string
     */
    public string $type;

    /**
     * @var Model
     */
    public Model $model;

    /**
     * @var string
     */
    public string $attribute;

    /**
     * Field constructor.
     * @param Model $model
     * @param string $attribute
     */
    public function __construct(Model $model, string $attribute)
    {
        $this->type = self::TYPE_TEXT;
        $this->model = $model;
        $this->attribute = $attribute;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        if( $this->type === self::TYPE_SELECT){
            return sprintf('
            <div class="form-group">
                <label for="exampleFormControlTextarea1">%s</label>
                <select name="%s" class="form-control %s"  id="">
                    <option value="0"> Select format</option>
                    <option value="DVD">DVD</option>
                    <option value="VHS">VHS</option>
                    <option value="Blu-Ray">Blu-Ray</option>
                </select>
                <div class="invalid-feedback">%s</div>
            </div>
            
            
        ', $this->model->labels()[$this->attribute] ?? $this->attribute,
                $this->attribute,
                $this->model->hasError($this->attribute) ? 'is-invalid' : '',
                $this->model->getFirstError($this->attribute)
            );
        }
        if( $this->type === self::TYPE_TEXTAREA){
            return sprintf('
            <div class="form-group">
                <label for="exampleFormControlTextarea1">Start write with commas ","</label>
                <textarea name="%s" class="form-control %s" id="%s"></textarea>
                <div class="invalid-feedback">%s</div>
            </div>
        ', $this->attribute,
                $this->model->hasError($this->attribute) ? 'is-invalid' : '',
                $this->attribute,
                $this->model->getFirstError($this->attribute)
            );
        }

        if( $this->type === self::TYPE_FILE){
            return sprintf('
            <div class="custom-file mb-3">
                <input type="file" name="%s" class="custom-file-input %s" id="validatedCustomFile" required>
                <label class="custom-file-label" for="validatedCustomFile">%s</label>
                <div class="invalid-feedback">%s</div>
            </div>
        ', $this->attribute,
                $this->model->labels()[$this->attribute] ?? $this->attribute,
                $this->model->hasError($this->attribute) ? 'is-invalid' : '',
                $this->model->getFirstError($this->attribute)
            );
        }

        return sprintf('
            <div class="form-group">
                <label for="">%s</label>
                <input type="%s"  name="%s" value="%s" class="form-control %s" id="%s" aria-describedby="emailHelp">
                <div class="invalid-feedback">%s</div>
            </div>
        ', $this->model->labels()[$this->attribute] ?? $this->attribute,
            $this->type,
            $this->attribute,
            $this->model->{$this->attribute},
            $this->model->hasError($this->attribute) ? 'is-invalid' : '',
            $this->attribute,
            $this->model->getFirstError($this->attribute)
        );
    }

    /**
     * @return $this
     */
    public function fieldTypeChange($type): Field
    {
        $this->type = $type;
        return $this;
    }
}