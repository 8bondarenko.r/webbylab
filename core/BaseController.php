<?php


namespace app\core;

/**
 * Class BaseController
 * @package app\core
 */
class BaseController
{
    /**
     * @var string
     */
    public string $layout = 'main';

    /**
     * @param string $layout
     */
    public function setLayout(string $layout)
    {
       $this->layout = $layout;
    }
    /**
     * @param string $view
     * @param array $params
     * @return array|false|string|string[]
     */
    public function render(string $view, array $params = [])
    {
        return Application::$app->router->renderView($view, $params);
    }



}