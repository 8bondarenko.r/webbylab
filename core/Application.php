<?php


namespace app\core;

/**
 * Class Application
 * @package app\core
 */
class Application
{
    /**
     * @var string
     */
    public static string $ROOT_PATH;

    /**
     * @var Router
     */
    public Router $router;

    /**
     * @var Request
     */
    public Request $request;

    /**
     * @var Response
     */
    public Response $response;
    /**
     * @var Application
     */
    public static Application $app;

    /**
     * @var BaseController
     */
    public BaseController $baseController;

    /**
     * @var Database
     */
    public Database $database;

    /**
     * @var Session
     */
    public Session $session;

    /**
     * @var DbModel|null
     */
    public ?DbModel $user;

    /**
     * @var string
     */
    public string $userClass;

    /**
     * @var string
     */
    public string $layout = 'main';

    /**
     * Application constructor.
     * @param $rootPath
     * @param array $config
     */
    public function __construct($rootPath ,array $config)
    {
        $this->userClass = $config['userClass'];
        self::$ROOT_PATH = $rootPath;
        $this->request = new Request();
        $this->response = new Response();
        $this->session = new Session();
        $this->router = new Router($this->request, $this->response);
        self::$app = $this;
        $this->database = new Database($config['db']);

        $userId = Application::$app->session->get('user');
        if ($userId) {
            $userClass = new $this->userClass();
            $key = $userClass->primaryKey();
            $this->user = $userClass->findOne([$key => $userId]);
        } else {
            $this->user = null;
        }
    }


    public function run()
    {
        echo $this->router->resolve();
    }



    /**
     * @param DbModel $user
     * @return bool
     */
    public function login(DbModel $user): bool
    {
        $this->user = $user;
        $className = get_class($user);
        $primaryKey = (new $className)->primaryKey();
        $value = $user->{$primaryKey};
        Application::$app->session->set('user', $value);

        return true;
    }

    /**
     * logout action
     */
    public function logout()
    {
        $this->user = null;
        self::$app->session->remove('user');
    }

    /**
     * @return bool
     */
    public function isGuest()
    {
        return !self::$app->user;
    }

}