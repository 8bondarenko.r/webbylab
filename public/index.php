<?php
use app\core\Application;
use app\src\controllers\AuthController;
use app\src\controllers\SiteController;

require_once dirname(__DIR__) . '../vendor/autoload.php';

$dotenv = Dotenv\Dotenv::createImmutable(dirname(__DIR__));
$dotenv->load();


$config = [
    'userClass' => \app\src\models\User::class,
    'db'=>['dsn' => $_ENV['DB_DSN'],'user' => $_ENV['DB_USER'],'password' => $_ENV['DB_PASSWORD'],]
];

$app = new Application(dirname(__DIR__), $config);

$app->router->get('/', [SiteController::class, 'home']);


$app->router->get('/upload', [SiteController::class, 'upload']);
$app->router->post('/upload', [SiteController::class, 'upload']);

$app->router->get('/add_movie', [SiteController::class, 'addMovie']);
$app->router->post('/add_movie', [SiteController::class, 'addMovie']);
$app->router->post('/remove_movie', [SiteController::class, 'removeMovie']);

$app->router->get('/login', [AuthController::class, 'login']);
$app->router->post('/login', [AuthController::class, 'login']);

$app->router->get('/logout', [AuthController::class, 'logout']);

$app->router->get('/register', [AuthController::class, 'register']);
$app->router->post('/register', [AuthController::class, 'register']);

$app->run();